# Curso de Markdown
![][i3]
2019 Daniel Aguilar Ramirez.
___
___
## 00 - Introducción  
**Lenguaje muy simple** de programacion, permite **escribir** de forma *facil y rapida.*  
1. Posteos de **blog**  
1. Archivos readme de **github**  
1. Mensajes en **trello**  
1. Mensajes en **slak**  
1. Mensajes en **whatsapp**  
1. Otros tipos de **docuementos**  
1. Permite **exportar** estos documentos **a otros formatos** como: html, pdf, doc, ect.  
[Video Referencia][00]
___ 
___ 
## 01 - Introduccion a VS Code   
Es necesario de un *procesador de textos* pudiendo emplear cualquiera de los mas populares, en este caso emplearemos **VS Code**.  
+ VS Code trae **incorporado** un procesador de markdown.  
+ Se **visualiza automaticamente** los cambios. 

**Pasos** para crear nuestro primer Markdown.  
1. Crear un archivo con la **extension .md**.   
1. Habilitar el procesador automatico de Markdo **habilitando la previzualizacion** en VS Code.  
[Video Referencia][01]
___
___ 
## 02 - Otros softwares  
Exsten **otros paquetes** para otros editores:  
+ **sublimeText**->MarkdonEditing  
+ **Atom**->markdown-writer  
+ **Vim**->Vim Markdown  
+ **mac**->Marked2, Mou, MacDown, MultiMarkdown.  
[Video Referencia][02] 
___
___     
## 03 - Decoración de textos  
+ Los **parrafos** se aplican dando *doble espacio*.  
+ Las letras en formato **italica** se aplican encerrando entre ** o *_* , *italica* o bien _italica_.  
\*texto*  
\_texto_  
+ Para el texto en **negritas**, se encierran entre *doble * o _*. **negritas** o tambien __negritas__.  
\**
texto**  
\__
texto__  
+ Para **tachar** el texto usamos.  
\~~
texto~~  
~~tachado~~.   
[Video Referencia][03]  
___
___
## 04 - Títulos
La forma mas sencilla de establecer un **titular** es empleando el **simbolo #** al comienzo de una linea. 
+ \# Titular1 
# Titular1
+ \## Titular2
## Titular2
+ \### Titular3
### Titular3
+ \#### Titular4
#### Titular4
+ \##### Titular5
##### Titular5
+ \###### Titular6 
###### Titular6
[Video Referencia][04]
___
___
## A - Insertar una linea
Para **insertar** una **linea** empleamos **___**.  
___
___
## B - Caracter de escape 
El **caracter de escape** empleado en Markdown es: \ .  
___ 
___ 
## 05 - Links  
Podemos insertar los link **directamente** escribiendo la **url** con **www**:  
https://www.youtube.com  
Para establecer una **mascara** a link:   
\[
Mascara]\(url)   
[youtube](www.youtube.com)  
Para establecer el **atributo title** al link  
\[
Mascara](url "titulo")  
[youtube](www.youtube.com "Es un sitio web dedicado a compartir videos.")  
Para Insertar un link **dentro de un parrafo** utilizamos una **referencia** que se agrega al final del documento.
\[Texto a mostrar][#referencia]   
\[Referencia]:[url] ("title")  
La pagina web de [Youtube][1] es un sitio web dedicado a compartir videos  
[Video de Referencia][05]
___
___
## 06 - Imágenes
Para **insertar** una **imagen** utilizamos la siguiente sintaxis:  
\!
\[
"texto_alternativo"](url_imagen "titulo")     
!["imagen1.jpg"](imagen1.png "mi imagen")   
Para insertar una **imagen con una referencia**.   
\![]\[2]   
![][i2]  
[Video de Referencia][06]  
Cambiar el **tamaño** de una imagen:  
\!
[](./pic/pic1_50.png =100x20)  
___
___
## 07 - Listas
**Listas Desordenadas**  
Para crear una lista desordenada solo se necesita agregar uno de los **caracteres *,+,-** y dar un espacio. 

\+ item 1  
\+ item 2  
\+ item 3   

Lista Desordenada 
+ item 1
+ item 2
+ item 3

**Listas Ordenardas**  

Para crear una lista desordenada solo se necesita **agregar 1.** y dar un espacio. 

\1. item 1  
\1. item 2  
\1. item 3 

Lista Ordenada:
1. item 1  
1. item 2
1. item 3  

Para Anidar elementos solo es necesario identar a 4 espacios por debajo del item padre.

**Lista Ordenada y Desordenada Anidada:**
1. item 1   
    1. Subitem 1.1  
    1. Subitem 1.2
1. item 2  
    + Subitem desordenado
        + subitem desordenado
1. item 3   
[Video de Referencia][07]
___
___
## 08 - Citas
Para crear una **cita** en Markdown basta con utilizar la sigiente sintaxis:  
\>  
\> nombre del autor o referencia  
ejemplo: 
"darme un punto de apoyo y movere al mundo"  
>
>*Arquímedes*  
[video de Referencia][08]
___
___
## 09 - Bloques de código
* Integrando bloque de codigo de programacion mediante identado  
    var variable = 10;  
*Esto no es muy visible en la practica*

* Integrando bloque de codigo **utilizando 3 comillas siples**.  
```
<p><em>This</em> is an HTML example!</p>
```
*sintaxis*  
\```
\<p>HTML example!</p>
\```    

* podemos integrar despues de las **tripes comillas simples**  escribiendo inmediatamente despues el **lenguaje del programacion** al aque hacemos **referencia**.


```js
var numero = 10;
```   
*sintaxis*  


\```js  
var numero = 10;  
\```


* Integrando un **bloque de codigo en un paraffo** mediente una simple comilla

Este es un parrafo con una variable `var numero2 = 10;`

*sintaxis* 

Este es un parrafo con una variable \`var numero2 = 10;`

**Ejercicio**

```php
var $nombre = "daniel";
echo $daniel;
```
*sintaxis*

\```php  
var $nombre = "daniel";  
echo $daniel;  
\```  

[Video de Referencia][09]

___
___
## 10 - Tablas
* Podemos **integrar tablas** mediante la siguiente *sintaxis*:

\|campo1\|campo\|  
\|---\|---\|  
\|fila1 campo1\|fila1 campo2\|  
\|fila2 campo1\|fila2 campo2\|  
\|fila3 campo1\|fila3 campo2\|  

la salida en pantalla seria la siguiente*

|campo1|campo|
|---|---|
|fila1 campo1|fila1 campo2|
|fila2 campo1|fila2 campo2|
|fila3 campo1|fila3 campo2|

* Podemos **alinear derecha el contenido tablas** mediante la siguiente *sintaxis*:

\|campo1\|campo\|  
\|---:\|---:\|  
\|fila1 campo1\|fila1 campo2\|  
\|fila2 campo1\|fila2 campo2\|  
\|fila3 campo1\|fila3 campo2\|  

la salida en pantalla seria la siguiente*

|campo1|campo|
|---:|---:|
|fila1 campo1|fila1 campo2|
|fila2 campo1|fila2 campo2|
|fila3 campo1|fila3 campo2|

* Podemos **alinear al centro el contenido tablas** mediante la siguiente *sintaxis*:

\|campo1\|campo\|  
\|:---:\|:---:\|  
\|fila1 campo1\|fila1 campo2\|  
\|fila2 campo1\|fila2 campo2\|  
\|fila3 campo1\|fila3 campo2\|  

la salida en pantalla seria la siguiente*

|campo1|campo|
|:---:|:---:|
|fila1 campo1|fila1 campo2|
|fila2 campo1|fila2 campo2|
|fila3 campo1|fila3 campo2|

[Video de Referencia][10]
___
___
## 11 - Pandoc
Para **exportar** a otro formato de archivo nuestro archivo markdow es necesario **intregrando una extension** a **VS Code** llamada **vscode-panoc** desde el panel de extenciones.

![][i4]

Tambien es necesario **instalar** desde la siguient direccion url el intalador de **pandoc**:

[https://pandoc.org/installing.html](https://pandoc.org/installing.html "website de pandoc")

Para realizar la exportacion es nesesario seguir los siguientes pasos:

1.  Seleccionar pandoc Render

![][i5]

[Video de Referencia][11]

2. Seleccionar el formato de exportacion

![][i6]

3. En la carpeta del proyecto se guarda el archivo exportado

* **Otra forma** mas sencilla de realizar la exportacion es cisitando algun **sitio web espcializado**. como:

[https://markdowntohtml.com](https://markdowntohtml.com "Convert Markdown to HTML")

1. Copiar y pegar el codigo Markdow.
1. Seleccionar el formato de salida.
1. Copiar y pergar el codigo de salida en un archivo con la extension del codigo generado.
___
___
## Video de 5 minutos que explica la sintaxis de markdown

Existe un video que explica de forma rapida u sencilla  la sintaxis:

Es recomendable visitar su sitio web:  [markdown.es][3]

[Video: markdown en 5 Minutos][2]

>
>Javier Cristóbal
>
___
___
## 
## Anexo final Refereacias para los links de este documento
\[1]: www.youtube.com  
\[00]: https://www.youtube.com/watch?v=IoX5Bmm-NT0 (Introducción)  
\[01]: https://www.youtube.com/watch?v=CyZ3w5Fa9OM (Introduccion a VS Code)  
\[02]: https://www.youtube.com/watch?v=dfsjQdpECo0 (Otros softwares)  
\[03]: https://www.youtube.com/watch?v=VaBLRcWR1DM (Decoración de textos)  
\[04]: https://www.youtube.com/watch?v=RxUjeD8jHyo (Títulos)  
\[05]: https://www.youtube.com/watch?v=nfOjPYZZJVc (Links)  
\[06]: https://www.youtube.com/watch?v=GLshtehqzj0 (Imágenes)  
\[07]: https://www.youtube.com/watch?v=i2gftCG9hvc (Listas)  
\[08]: https://www.youtube.com/watch?v=gnLca-unMvY (Citas)  
\[09]: https://www.youtube.com/watch?v=nv55BHU6j6s (Bloques de código)  
\[10]: https://www.youtube.com/watch?v=q4h54-fuVas (Tablas)  
\[11]: https://www.youtube.com/watch?v=XZDgWf94f5I (Pandoc)  
\[i2]: imagen2.png (title)  
\[i3]: imagen3.png (Markdown Logo)  
\[i4]: imagen4.png (vscode-pandoc)  
\[i5]: imagen5.png (pandoc Render)  
\[i6]: imagen6.png (pandoc formato)   
\[2]: https://www.youtube.com/watch?time_continue=5&v=y6XdzBNC0_0 (Aprende markdown en 5 Minutos)   
\[3]: https://markdown.es/ (markdown.es)
___
___

[1]: www.youtube.com
[00]: https://www.youtube.com/watch?v=IoX5Bmm-NT0 (Introducción)
[01]: https://www.youtube.com/watch?v=CyZ3w5Fa9OM (Introduccion a VS Code)
[02]: https://www.youtube.com/watch?v=dfsjQdpECo0 (Otros softwares)
[03]: https://www.youtube.com/watch?v=VaBLRcWR1DM (Decoración de textos)
[04]: https://www.youtube.com/watch?v=RxUjeD8jHyo (Títulos)
[05]: https://www.youtube.com/watch?v=nfOjPYZZJVc (Links)
[06]: https://www.youtube.com/watch?v=GLshtehqzj0 (Imágenes)
[07]: https://www.youtube.com/watch?v=i2gftCG9hvc (Listas)
[08]: https://www.youtube.com/watch?v=gnLca-unMvY (Citas)
[09]: https://www.youtube.com/watch?v=nv55BHU6j6s (Bloques de código)
[10]: https://www.youtube.com/watch?v=q4h54-fuVas (Tablas)
[11]: https://www.youtube.com/watch?v=XZDgWf94f5I (Pandoc)
[i2]: imagen2.png (title)
[i3]: imagen3.png (Markdown Logo)
[i4]: imagen4.png (vscode-pandoc)
[i5]: imagen5.png (pandoc Render)
[i6]: imagen6.png (pandoc formato)
[2]: https://www.youtube.com/watch?time_continue=5&v=y6XdzBNC0_0 (Aprende markdown en 5 Minutos) 
[3]: https://markdown.es/ (markdown.es)

